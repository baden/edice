-module(edice_player).
-behaviour(gen_fsm).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0, start_link/1]).

%% ------------------------------------------------------------------
%% gen_fsm Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_event/3,
         handle_sync_event/4, handle_info/3, terminate/3,
         code_change/4]).

-export([lobby/3, inroom/2, inroom/3, wait_credentials/2, wait_credentials/3, loged_in/2, loged_in/3]).
-export([set_credentials/2, report/1, get/1, command/3, subscribe/1, send_update/1, where/1]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_fsm:start_link(?MODULE, [], []).

start_link([Username, Password]) ->
    gen_fsm:start_link(?MODULE, [Username, Password], []).


-record(state, {
    startat,
    id,
    username,
    password,
    room
}).

%% ------------------------------------------------------------------
%% gen_fsm Function Definitions
%% ------------------------------------------------------------------

init([Username, Password]) ->
    process_flag(trap_exit, true),
    gproc:reg({p, l, players}, Username),   % Для хранения связки pid -> username
    gproc:reg({n, l, {player, Username}}),  % Для определения pid уже существующих игроков
    gproc:reg({n, l, {player, self()}}, #{
        username => Username
    }),
    % Три назначения кажутся избыточными
    moveto(lobby),
    State = #state{
        startat = edice:unixtime(),
        id = edice:uuid(<<"Player:">>),
        username = Username,
        password = Password,
        room = whereis(lobby)
    },
    % TODO: Только для observer, в продакшине убрать!!!
    register(binary_to_atom(<<"Player:", Username/binary>>, utf8), self()),
    {ok, lobby, State}.

set_credentials(Pid, [Username, Password]) when is_pid(Pid) ->
    gen_fsm:send_event(Pid, {set_credentials, Username, Password}).

report(Pid) ->
    gen_fsm:sync_send_all_state_event(Pid, {report}).

get('God') ->
    #{ username => <<"God">>};
    
get(Pid) ->
    gproc:lookup_value({n, l, {player, Pid}}).

command(Pid, Command, Data) when is_pid(Pid) ->
    gen_fsm:sync_send_event(Pid, {command, Command, Data}).

subscribe(Pid) ->
    gproc_ps:subscribe(l, {player, Pid}).

send_update(Pid) ->
    % self() - указывает на ws_handler
    gproc_ps:publish(l, {player, Pid}, {update}).

where(Pid) ->
    gen_fsm:sync_send_all_state_event(Pid, {where}).
% States

lobby({command, <<"create_room">>, _Data}, _From, State) ->
    {ok, Pid} = edice_room_sup:start_child(self()),
    {ok, Room} = edice_room:report(Pid),
    moveto(State#state.room, Pid),
    Result = [
        {room, Room}
    ],
    {reply, Result, inroom, State#state{room = Pid}};

lobby({command, <<"join_room">>, #{target := Pid}}, _From, State) ->
    RoomPid = list_to_pid(binary_to_list(Pid)),
    {ok, Room} = edice_room:report(RoomPid),
    moveto(State#state.room, RoomPid),
    Result = [
        {room, Room}
    ],
    {reply, Result, inroom, State#state{room = RoomPid}};

lobby(Event, _From, State) ->
    lager:warning("edice_room:lobby(~p, ~p, ~p)", [Event, _From, State]),
    {reply, [{answer, <<"error">>}], lobby, State}.

% lobby({command, Command}, State) ->
%     lager:warning("edice_room:lobby({command, ~p})", [Command]),
%     {next_state, lobby, State}.

inroom(Event, State) ->
    lager:warning("edice_player:inroom(~p, ~p)", [Event, State]),
    {next_state, inroom, State}.

inroom({command, <<"exit_room">>, _Data} = _Event, _From, State) ->
    moveto(State#state.room, lobby),
    % gproc_ps:subscribe(l, {room, lobby}),
    {reply, [{answer, <<"ok">>}], lobby, State#state{room = whereis(lobby)}};

inroom(Event, From, State) ->
    lager:warning("edice_player:inroom(~p, ~p, ~p)", [Event, From, State]),
    {reply, [{answer, <<"error">>}], inroom, State}.

wait_credentials({set_credentials, Username, Password}, State) ->
    lager:warning("edice_player:wait_credentials({set_credentials, ~p, ~p}, ~p)", [Username, Password, State]),
    {next_state, loged_in, State#state{username=Username, password=Password}};

wait_credentials(_Event, State) ->
    lager:warning("edice_room:wait_credentials(~p, ~p) (Unexpected)", [_Event, State]),
    {next_state, wait_credentials, State}.

wait_credentials(_Event, _From, State) ->
    lager:warning("edice_room:wait_credentials(~p, ~p, ~p)", [_Event, _From, State]),
    {reply, ok, wait_credentials, State}.


loged_in(_Event, State) ->
    lager:info("edice_room:loged_in(~p, ~p)", [_Event, State]),
    {next_state, loged_in, State}.

loged_in(_Event, _From, State) ->
    lager:info("edice_room:loged_in(~p, ~p, ~p)", [_Event, _From, State]),
    {reply, ok, loged_in, State}.

handle_event(_Event, StateName, State) ->
    lager:info("edice_player:handle_event(~p, ~p, ~p)", [_Event, StateName, State]),
    {next_state, StateName, State}.

handle_sync_event({report}, _From, StateName, State) ->
    Report = [
        {id, State#state.id},
        {pid, self()},
        % {from, _From}, % ws_handler
        {room, State#state.room},
        {startat, State#state.startat},
        {username, State#state.username},
        {state, StateName}
        % {password, State#state.password}
    ],
    {reply, {ok, Report}, StateName, State};

handle_sync_event({where}, _From, StateName, State = #state{room = Room}) ->
    {reply, {ok, Room}, StateName, State};

handle_sync_event(_Event, _From, StateName, State) ->
%% ------------------------------------------------------------------
    lager:info("edice_player:handle_sync_event(~p, ~p, ~p, ~p)", [_Event, _From, StateName, State]),
    {reply, ok, StateName, State}.

handle_info({credentials, [Username, Password]}, StateName, State) ->
    lager:info("edice_player:handle_info({credentials, [~p, ~p]}, ~p, ~p)", [Username, Password, StateName, State]),
    {next_state, loged_in, State#state{username=Username, password=Password}};

handle_info({gproc_ps_event, {room, RoomPid}, {update, Data}}, StateName, State) ->
    % io:format("- - P>~n",[]),
    NewData = try
        player_data({StateName, Data, State, RoomPid})
    catch
        Target:Reason ->
            lager:error("Error build player_data({~p,?,~p,~p}) reason (~p:~p) stacktrace: ~p", [StateName, State, RoomPid, Target, Reason, erlang:get_stacktrace()]),
            % erlang:display(erlang:get_stacktrace()),
            Data
    end,
    % lager:info("   >> Data = ~p", [io_lib:format("~p", [Data])]),
    gproc_ps:publish(l, {player, self()}, {update, NewData}),
    % io:format("- - P<~n",[]),
    {next_state, StateName, State};


handle_info(_Info, StateName, State) ->
    lager:warning("edice_player:handle_info(~p, ~p, ~p)", [_Info, StateName, State]),
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    lager:info("edice_player:terminate(~p, ~p, ~p)", [_Reason, _StateName, _State]),
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.



%% ------------------------------------------------------------------
%% Internal Function Definitions

player_data({StateName, Data, State, _RoomPid}) ->
    % erlang:error(self, {player, self()}),
    #{room := Room} = Data,
    % io:format("Boo~n", []),
    % Рекурсивное наполнение данными
    NewData = Data#{
        player => #{
            pid     => self(),
            id      => State#state.id,
            % {from, _From}, % ws_handler
            room    => State#state.room,
            startat => State#state.startat,
            username => State#state.username,
            state   =>  StateName
        },
        room => Room#{      % extend
            caller  => self(),
            data    => edice_room:private_room_data(State#state.room)
        }
    },
    % lager:info("   >> Room = ~p", [Room]),
    NewData.


movefrom(lobby) ->
    movefrom(whereis(lobby));

movefrom(RoomFrom) ->
    edice_room:leave(RoomFrom),
    edice_room:send_update(RoomFrom).

moveto(lobby) ->
    moveto(whereis(lobby));

moveto(RoomTo) ->
    edice_room:enter(RoomTo),
    edice_room:send_update(RoomTo).

moveto(RoomFrom, RoomTo) ->
    movefrom(RoomFrom),
    moveto(RoomTo).
