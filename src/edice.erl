-module(edice).

-export([start/0, stop/0]).

-export([unixtime/0, uuid/1]).


% Manual start by erl -pa ebin deps/*/ebin -s edice
start() ->
    io:format("edice:start()~n", []),
    application:load(edice),
    {ok, Apps} = application:get_key(edice, applications),
    io:format("Apps = ~p~n", [Apps]),
    [startit(App) || App <- Apps],
    % application:ensure_all_started(edice).
    % ok = ensure_started(crypto),
    % ok = ensure_started(lager),
    % ok = ensure_started(ranch),
    % ok = ensure_started(cowboy),
    % ok = ensure_started(edice).
    ok = application:start(edice),

    % ok = sync:go().
    ok.

stop() ->
    io:format("edice:stop()~n"),
    Res = application:stop(edice),
    Res.

startit(App) ->
    io:format("   starting ~p: ", [App]),
    Res = application:ensure_all_started(App),
    io:format("      ~p~n", [Res]),
    ok.

% ensure_started(App) ->
%     case application:start(App) of
%         ok -> ok;
%         {error, {already_started, App}} -> ok
%     end.

unixtime() ->
    timer:now_diff(now(), {0, 0,0 }) div 1000000.

uuid(Base) when is_binary(Base) ->
    P1 = integer_to_binary(unixtime()),
    P2 = integer_to_binary(crypto:rand_uniform(1, trunc(math:pow(2, 32)))),
    % P2 = base64:encode(crypto:rand_bytes(8)),
    base64:encode(<<Base/binary, $:, P1/binary, $:, P2/binary>>).
