
-module(edice_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I), {I, {I, start_link, []}, permanent, 5000, worker, [I]}).

-define(APP, edice).

-define(SERVER, ?MODULE).
-define(MAX_RESTART, 5). %% Max restarts
-define(MAX_TIME, 60).   %% Seconds between restarts

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    lager:info("edice_sup:start_link()", []),
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    lager:info("edice_sup:init()", []),
    {ok, _} = cowboy:start_http(http, 3, [{port, config(api_port, 8100)}],
                                           [{env, [{dispatch, dispatch_rules()}]}]),

    lager:info("Start edice_rooms server", []),
    % {ok, { {one_for_one, 5, 10}, [?CHILD(edice_rooms)]} }.
    Flags = {one_for_one, ?MAX_RESTART, ?MAX_TIME},
    % RoomsWorker = {
    %     edice_rooms,                       %% Id
	% 	{edice_rooms, start_link, []}, %% StartFunc = {M, F, A}
	% 	permanent,                          %% Permanent - child process is always restarted.
	% 	5000,                               %% Defines how a child process should be terminated. 2000 - timeout befor terminated.
	% 	worker,                             %% Type of child (worker | supervisor).
	% 	[edice_rooms]                       %% Callback module, shuld be a list with one element.
	% },
    RoomSupervisor = {
        ediceRoomPoolSup,
        {edice_room_sup, start_link, []},
		permanent,
		infinity,
		supervisor,
		[edice_room_sup]
	},
    % PlayersWorker = {
    %     edice_players,                       %% Id
    %     {edice_players, start_link, []}, %% StartFunc = {M, F, A}
    %     permanent,                          %% Permanent - child process is always restarted.
    %     5000,                               %% Defines how a child process should be terminated. 2000 - timeout befor terminated.
    %     worker,                             %% Type of child (worker | supervisor).
    %     [edice_players]                       %% Callback module, shuld be a list with one element.
    % },
    PlayerSupervisor = {
        edicePlayerPoolSup,
        {edice_player_sup, start_link, []},
        permanent,
        infinity,
        supervisor,
        [edice_player_sup]
    },
    % {ok, {Flags, [RoomsWorker, RoomSupervisor, PlayersWorker, PlayerSupervisor]}}.
    {ok, {Flags, [RoomSupervisor, PlayerSupervisor]}}.

dispatch_rules() ->
    cowboy_router:compile(
        [{'_', [
            {"/", cowboy_static, {file, "priv/index.html"}},
            {"/js/[...]", cowboy_static, {dir, "priv/js"}},
            {"/css/[...]", cowboy_static, {dir, "priv/css"}},
			% {"/ws", chat_ws_handler, []},
            % {"/assets/[...]", cowboy_static, {priv_dir, my_app, "priv/assets"}},
            % {"/", index_handler, []}
            % {"/info", api_info, []}
            % {"/rest/:resource", rest_cowboy, []},
            % {"/rest/:resource/:id", rest_cowboy, []},
            % {"/ws/[...]", bullet_handler, [{handler, n2o_bullet}]},
            {"/websocket", ws_handler, []},
			{'_', cowboy_static, {file, "priv/404.html"}}
    ]}]).

% mime() -> [{mimetypes,cow_mimetypes,all}].

config(Key, Default) ->
    io:format("edice:config(~p, ~p)~n", [Key, Default]),
    case application:get_env(?APP, Key) of
        undefined -> Default;
        {ok, Val} -> Val
    end.
