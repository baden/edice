-module(edice_room_sup).
-behaviour(supervisor).
-export([start_link/0, start_child/1, init/1]).

-export([report/0]).

-define(SERVER, ?MODULE).

start_link() ->
    lager:info("edice_room_sup:start_link()", []),
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

% start_child(lobby) ->

start_child(PlayerPid) ->
    lager:info("edice_room_sup:start_child(~p)", [PlayerPid]),
	supervisor:start_child(edice_room_sup, [PlayerPid]).

report() ->
    lists:map(fun({_, Pid, _, _}) ->
        {ok, Data} = edice_room:report(Pid),
        Data
    end, supervisor:which_children(?SERVER)).


init(_Args) ->
    lager:info("edice_room_sup:init(~p)", [_Args]),
    % Создадим общую комнату, приемную (lobby)
    {ok, _Lobby} = edice_room:start_link('God'),

    ClientsSpecs = {
        undefined,
        {edice_room, start_link, []},
        % permanent,
        % infinity,
        temporary,
        1,
        % 2000,
        worker,
        [edice_room]
    },
    {ok, {{simple_one_for_one, 1, 1}, [ClientsSpecs]}}.
