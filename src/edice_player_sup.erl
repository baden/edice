-module(edice_player_sup).
-behaviour(supervisor).
-export([start_link/0, start_child/1, init/1]).
-export([report/0]).

-define(SERVER, ?MODULE).

start_link() ->
    lager:info("edice_player_sup:start_link()", []),
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

start_child(Args = [Username, _Password]) ->
    case gproc:where({n, l, {player, Username}}) of
        undefined ->
            supervisor:start_child(?SERVER, [Args]);
        Pid ->
            {ok, RoomPid} = edice_player:where(Pid),
            edice_room:send_update(RoomPid),
            {ok, Pid}
    end.
    % supervisor:start_child(?SERVER,
    %                                  {edice_player, {edice_player, start_link, [Args]},
    %                                   transient, 100, worker, [edice_player]}).

% find_player([Username, _Password]) ->
%     % _All = supervisor:which_children(?SERVER),
%     gproc:where({n, l, Username}),
%     ok.

report() ->
    lists:map(fun({_, Pid, _, _}) ->
        {ok, Data} = edice_player:report(Pid),
        Data
    end, supervisor:which_children(?SERVER)).

init(_Args) ->

    ClientsSpecs = {
        undefined,
        {edice_player, start_link, []},
        permanent,
        infinity,
        % temporary,
        % 2000,
        worker,
        [edice_player]
    },
    {ok, {{simple_one_for_one, 1, 1}, [ClientsSpecs]}}.
