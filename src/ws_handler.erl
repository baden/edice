%% -*- coding: utf-8 -*-
-module(ws_handler).
-behaviour(cowboy_websocket_handler).

-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-record(state, {
    player_pid = undefined
}).

init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_websocket}.

websocket_init(_TransportName, Req, _Opts) ->
    gproc:reg({p, l, ws_room}), % Не знаю, понадобится ли эта подписка. Что такого можно слать всем сразу?
    % И встроенный механизм pg2 делает тоже самое: http://erlang.org/doc/man/pg2.html

    Players = edice_player_sup:report(),
    Rooms = edice_room_sup:report(),
    Report = [{players, Players}, {rooms, Rooms}],
    self() ! {json, Report},
    % erlang:start_timer(100, self(), Wellcome),
    {ok, Req, #state{}}.
    % {ok, Req, #state{}, 600000, hibernate}. % Test me latter

websocket_handle({text, Msg}, Req, State) ->
    case jsx:is_json(Msg) of
		true ->
			Data = maps:from_list(jsx:decode(Msg, [{labels, atom}])),
			{_Result, NewState} = message_handle(Data, State),
            {ok, Req, NewState};
		false ->
			lager:error("Msg is not json: [~p]", [Msg]),
            {ok, Req, State}
	end;

websocket_handle(_Data, Req, State) ->
    lager:warning("ws_handler:websocket_handle(~p, ?, ~p)", [_Data, State]),
    {ok, Req, State}.

websocket_info({timeout, _Ref, Msg}, Req, State) ->
    lager:warning("ws_handler:websocket_info({timeout, ~p, ~p}, ?, ~p)", [_Ref, Msg, State]),
    {reply, {text, Msg}, Req, State};

websocket_info({text, Msg}, Req, State) ->
    lager:warning("ws_handler:websocket_info({text, ~p}, ?, ~p)", [Msg, State]),
    {reply, {text, Msg}, Req, State};

websocket_info({json, Msg}, Req, State) ->
    Json = jsx:encode(pre_encode(Msg)),
    {reply, {text, Json}, Req, State};

websocket_info({gproc_ps_event, {player, PlayerPid}, {update, Data}}, Req, State) ->
    % io:format("- - - W>~n",[]),

    % В теории, тут можно еще досыпать информации, относящейся к конкретному подключению.
    % Это в качестве демонстрации, практической ценности это не имеет.
    % gproc_ps:publish(l, {player, self()}, {update, NewData}),
    Result = Data#{
        connection => #{
            all => gproc:lookup_pids({p, l, ws_room}),
            aliases => [
                Pid || Pid <- gproc_ps:list_subs(l, {player, PlayerPid}), Pid =/= self()
            ],
            req => connection_info(Req)
        }
    },
    Json = jsx:encode(pre_encode(Result)),
    % Json = <<"{}">>,
    % io:format("- - - W<~n",[]),
    {reply, {text, Json}, Req, State};

websocket_info(_Info, Req, State) ->
    lager:warning("ws_handler:websocket_info(~p, ?, ~p)", [_Info, State]),
    {ok, Req, State}.

websocket_terminate(_Reason, _Req, _State) ->
    ok.

message_handle(#{auth := Credentials}, State) ->
    % lager:info("auth ~p", [Credentials]),
    User = proplists:get_value(username, Credentials),
    Password = proplists:get_value(password, Credentials),
    {ok, Pid} = edice_player_sup:start_child([User, Password]),
    % Привяжем канал к игроку
    edice_player:subscribe(Pid),
    Result = [
        {pid, self()}
    ],
    {Result, State#state{player_pid = Pid}};


message_handle(Data = #{command := Command}, State = #state{player_pid = PlayerPid}) ->
    edice_player:command(PlayerPid, Command, Data),
    {ok, State};

message_handle(#{update := Resource}, State = #state{player_pid = PlayerPid}) ->
    update(Resource, PlayerPid),
    {ok, State};

message_handle(#{chat := Chat}, State = #state{player_pid = PlayerPid}) ->
    chat(PlayerPid, Chat),
    {ok, State};

message_handle(Data, State) ->
    lager:warning("Uexpected ws data: ~p", [Data]),
    {ok, State}.
    % Command = proplists:get_value(command, Data),
    % message_handle(Command, Data, State).

% message_handle(Command, Data, State = #state{player_pid = PlayerPid}) ->
    % edice_player:command(PlayerPid, Command, Data),
    % {ok, State}.

update(<<"players">>, _PlayerPid) ->
    Players = edice_player_sup:report(),
    Report = [{players, Players}],
    self() ! {json, Report},
    ok;

update(<<"rooms">>, _PlayerPid) ->
    Rooms = edice_room_sup:report(),
    Report = [{rooms, Rooms}],
    self() ! {json, Report},
    ok;

update(Resource, _PlayerPid) ->
    lager:warning("Request unsupported resource: ~p", [Resource]),
    ok.

% Чат. Пока тупо возвращаем эхо
chat(PlayerPid, Chat) ->
    Message = #{
        chat => #{
            sender => edice_player:get(PlayerPid),
            text => Chat
        }
    },
    % self() ! {json, Message},
    gproc:send({p, l, ws_room}, {json, Message}),
    ok.

% Это кажется глупым ради одного типа is_pid()
pre_encode({[]}) ->
    [{}];
pre_encode({PropList}) ->
    pre_encode(PropList);
pre_encode([{_, _}|_] = PropList) ->
    [ {Key, pre_encode(Value)} || {Key, Value} <- PropList ];
pre_encode(Map) when is_map(Map) ->
    pre_encode(maps:to_list(Map));
pre_encode(List) when is_list(List) ->
    [ pre_encode(Term) || Term <- List ];
pre_encode(true) ->
    true;
pre_encode(false) ->
    false;
pre_encode(null) ->
    null;
pre_encode(Pid) when is_pid(Pid) ->
    unicode:characters_to_binary(io_lib:format("~p", [Pid]));
pre_encode(Atom) when is_atom(Atom) ->
    erlang:atom_to_binary(Atom, utf8);
pre_encode(Term) when is_integer(Term); is_float(Term); is_binary(Term) ->
    Term.

connection_info(Req) ->
    Socket = cowboy_req:get(socket, Req),
    {ok,{Ip, Port}} = inet:peername(Socket),
    #{
        pid         => self(),
        ip          => list_to_binary(inet_parse:ntoa(Ip)),
        port        => Port,
        bindings    => list_to_binary(io_lib:format("~p", [cowboy_req:get(bindings, Req)])),
        % bindings    => cowboy_req:bindings(Req),
        % headers     => list_to_binary(io_lib:format("~p", [cowboy_req:get(headers, Req)])),
        headers     => cowboy_req:get(headers, Req),
        host        => cowboy_req:get(host, Req),
        host_info   => cowboy_req:get(host_info, Req),
        qs          => cowboy_req:get(qs, Req),
        path        => cowboy_req:get(path, Req),
        method      => cowboy_req:get(method, Req),
        version     => cowboy_req:get(version, Req)
        % peer        => cowboy_req:get(peer, Req),
        % raw         => list_to_binary(io_lib:format("~p", [Req]))
    }.
