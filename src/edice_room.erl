-module(edice_room).
-behaviour(gen_fsm).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0, start_link/1]).

%% ------------------------------------------------------------------
%% gen_fsm Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_event/3,
         handle_sync_event/4, handle_info/3, terminate/3,
         code_change/4]).

-export([report/1, send_update/1, private_room_data/1, enter/1, leave/1, players/1]).
-export([lobby/3, wait_players/2, wait_players/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    lager:info("edice_room:start_link()", []),
    gen_fsm:start_link(?MODULE, ['God'], []).


start_link(PlayerPid) ->
    lager:info("edice_room:start_link(~p)", [PlayerPid]),
    gen_fsm:start_link(?MODULE, [PlayerPid], []).

-record(state, {
    startat,
    id,
    owner
}).

%% ------------------------------------------------------------------
%% gen_fsm Function Definitions
%% ------------------------------------------------------------------

init([OwnerPlayerPid]) ->
    lager:info("edice_room:init(~p)", [OwnerPlayerPid]),
    process_flag(trap_exit, true),
    gproc:reg({p, l, rooms}),       % Нужен ли список комнат?
    State = #state{
        startat = edice:unixtime(),
        id = edice:uuid(<<"Room">>),
        owner = OwnerPlayerPid
    },
    case OwnerPlayerPid of
        'God' ->
            register(lobby, self()),
            {ok, lobby, State};
        _ -> {ok, wait_players, State}
    end.

report(undefined) ->
    {ok, [
        {id, null},
        {pid, null},
        {startat, null},
        {state, null},
        {owner, null}
    ]};

report(Pid) ->
    gen_fsm:sync_send_all_state_event(Pid, {report}).


send_update(Pid) ->
    % self() - указывает на ws_handler
    % io:format("P>~n",[]),
    % Так и не понял что лучше:
    Pid ! {update}.
    % gen_fsm:send_all_state_event(Pid, {update}).
    % io:format("P<~n",[]).

enter(Pid) ->
    try
        gproc_ps:subscribe(l, {room, Pid})
    catch
        _:_ ->
            lager:warning("Error subscribe to room ~p", [Pid])
    end,
    ok.

leave(Pid) ->
    try
        gproc_ps:unsubscribe(l, {room, Pid}),
        case gproc_ps:list_subs(l, {room, Pid}) of
            [] ->
                % Pid ! {destroy};
                gen_fsm:send_all_state_event(Pid, {destroy});
            _ -> ok
        end
        % lager:info("   > Res=~p", [Res])
    catch
        _:_ ->
            lager:warning("Error unsubscribe from room ~p", [Pid])
    end,
    ok.

players(Pid) ->
    gproc_ps:list_subs(l, {room, Pid}).

private_room_data(Pid) ->
    gen_fsm:sync_send_all_state_event(Pid, {room_data}).

% States

wait_players(_Event, State) ->
    lager:warning("edice_room:wait_players(~p, ~p)", [_Event, State]),
    {next_state, wait_players, State}.

lobby(_Event, _From, State) ->
    lager:warning("edice_room:lobby(~p, ~p, ~p)", [_Event, _From, State]),
    {reply, ok, lobby, State}.

wait_players(_Event, _From, State) ->
    lager:warning("edice_room:wait_players(~p, ~p, ~p)", [_Event, _From, State]),
    {reply, ok, wait_players, State}.

handle_event({update}, StateName, State) ->
    handle_info({update}, StateName, State);

handle_event({destroy}, _StateName, State) ->
    % handle_info({update}, StateName, State);
    lager:info("   Destroy room ~p", [self()]),
    {stop, normal, State};

handle_event(_Event, StateName, State) ->
    lager:warning("edice_room:handle_event(~p, ~p, ~p)", [_Event, StateName, State]),
    {next_state, StateName, State}.

handle_sync_event({report}, _From, StateName, State) ->
    Players = gproc_ps:list_subs(l, {room, self()}),
    % Players = [
    %     edice_player:get(PlayerPid) ||
    %     PlayerPid <- players(self())
    % ],

    Report = [
        {id, State#state.id},
        {pid, self()},
        {startat, State#state.startat},
        {state, StateName},
        {owner, edice_player:get(State#state.owner)},
        % {data, room_data({StateName, State})},
        {players, Players}
        % {password, State#state.password}
    ],
    {reply, {ok, Report}, StateName, State};

handle_sync_event({room_data}, {PlayerPid, _}, StateName, State) ->
    Result = try
        room_data({StateName, State, PlayerPid})
    catch
        Target:Reason ->
            lager:error("Error build room_data({~p,?,~p,~p}) reason (~p:~p) stacktrace: ~p", [StateName, State, PlayerPid, Target, Reason, erlang:get_stacktrace()]),
            % erlang:display(erlang:get_stacktrace()),
            #{ error => <<"catch in edice_room">>}
    end,
    {reply, Result, StateName, State};

handle_sync_event(_Event, _From, StateName, State) ->
    lager:warning("edice_room:handle_sync_event(~p, ~p, ~p, ~p)", [_Event, _From, StateName, State]),
    {reply, ok, StateName, State}.

handle_info({update}, StateName, State) ->
    % io:format("- R>~n",[]),
    % Часть данных обновления будет заполняться рекурсивно
    % Глобальная часть, не относящаяся к конкретному игроку
    % Players = gproc_ps:list_subs(l, {room, self()}),

    Players = [
        edice_player:get(PlayerPid) ||
        PlayerPid <- players(self())
    ],

    % erlang:error(self, {room, self()}),
    Data = #{
        room => #{
            id      => State#state.id,
            pid     => self(),
            startat => State#state.startat,
            state   => StateName,
            owner   => State#state.owner,
            % data    => room_data({StateName, State})},
            players => Players
        }
    },
    gproc_ps:publish(l, {room, self()}, {update, Data}),
    % [PlayerPid ! {gproc_ps_event, {room, self()}, {update, Data}} || PlayerPid <- gproc_ps:list_subs(l, {room, self()})],
    % io:format("- R<~n",[]),
    {next_state, StateName, State};

handle_info(_Info, StateName, State) ->
    lager:warning("edice_room:handle_info(~p, ~p, ~p)", [_Info, StateName, State]),
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    lager:info("edice_room:terminate(~p, ~p, ~p)", [_Reason, _StateName, _State]),
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

room_data({lobby, _State, _PlayerPid}) ->
    CreateGameButton = [
        {id, <<"command1">>},
        {type, <<"command_button">>},
        {data, [
            {command, <<"create_room">>}
        ]}
    ],
    % Usernames = gproc:lookup_values({p, l, players}),
    Rooms = [
        begin
            RoomId = list_to_binary(io_lib:format("~p", [Pid])),
            % Players = gproc_ps:list_subs(l, {room, Pid}),
            Players = [
                edice_player:get(PlayerPid) ||
                PlayerPid <- players(Pid)
            ],

        [
            {id, <<"Room:", RoomId/binary>>},
            {type, <<"room">>},
            {pid, Pid},
            {players, Players}
            % {data, report(Pid)}     % !!!! Потенциальный вечный цикл
        ]
        end || Pid <- gproc:lookup_pids({p, l, rooms}), Pid =/= self()
    ],
    #{
        create_room_command => CreateGameButton,
        rooms_list => Rooms
    };

room_data({wait_players, _State = #state{owner = _PlayerPid, _ = _}, _PlayerPid}) ->
    % ChoseMapList
    Label = #{
        type => <<"label">>,
        data => #{title => <<"owner">>}
    },
    ExitGameButton = #{
        id => <<"command1">>,
        type => <<"command_button">>,
        data => #{command => <<"exit_room">>}
    },
    [Label, ExitGameButton];

room_data({wait_players, _State, _PlayerPid}) ->
    % ChoseMapList
    ExitGameButton = #{
        id => <<"command1">>,
        type => <<"command_button">>,
        data => #{command => <<"exit_room">>}
    },
    [ExitGameButton];

room_data({StateName, _State, PlayerPid}) ->
    [#{
        type        => <<"unexpected_condition">>,
        statename   => StateName,
        playerpid   => PlayerPid
    }].
