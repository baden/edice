-module(edice_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    lager:info("edice_app:start(~p, ~p)", [_StartType, _StartArgs]),
    Sup = edice_sup:start_link(),
    % % Создадим общую комнату, приемную (lobby)
    % {ok, _Lobby} = edice_room_sup:start_child('God'),
    % register(lobby, Lobby),
    Sup.

stop(_State) ->
    lager:info("edice_app:stop(~p)", [_State]),
    ok.
