%% -*- coding: utf-8 -*-
-module(api_info).

-export([init/3]).
-export([options/2]).
-export([content_types_provided/2]).
-export([hello_to_html/2]).
-export([hello_to_json/2]).

% -include("types.hrl").

init(_Transport, _Req, []) ->
    lager:info("api_info:init(~p, ~p)", [_Transport, _Req]),
    {upgrade, protocol, cowboy_rest}.

options(Req, State) ->
    Req1 = cowboy_req:set_resp_header(<<"Access-Control-Allow-Methods">>, <<"GET, OPTIONS">>, Req),
    Req2 = cowboy_req:set_resp_header(<<"Access-Control-Allow-Origin">>, <<"*">>, Req1),
    {ok, Req2, State}.

content_types_provided(Req, State) ->
    {[
        {<<"text/html">>, hello_to_html},
        {<<"application/json">>, hello_to_json}
    ], Req, State}.

hello_to_html(Req, State) ->
    Body = <<"<html><head><meta charset=\"utf-8\"><title>API info</title></head><body><p>REST methods: GET</p></body></html>">>,
    {Body, Req, State}.

hello_to_json(Req, State) ->
    Req1 = cowboy_req:set_resp_header(<<"Access-Control-Allow-Origin">>, <<"*">>, Req),

    % Stats = stats:get(),

    Result = {[
        {info, <<"TBD">>}
    ]},

    % Body = jiffy:encode(Result),
    Body = jsx:encode(Result, [space, {indent, 4}, relax]),

    {Body, Req1, State}.
