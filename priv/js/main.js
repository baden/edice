var link = null;
var resource = null;

function InitLink(){
    var login = q$("#login");
    var cache = new Cache(true);
    var username = cache.get("username");
    var password = cache.get("password");

    if(username){
        q$("#loginas").innerHTML = username;
        resource = new Resource(username, password);
        link = new Link(resource);
        link.connect(1);
    } else {
        login.style.display = "";
        login.onsubmit = function(ev) {
            username = ev.target.elements.username.value;
            password = ev.target.elements.password.value;
            // console.log("On submit", ev, username, password);
            cache.set("username", username);
            cache.set("password", password);
            location.reload();
            // q$("#loginas").innerHTML = username;
            // login.style.display = "none";
            return false;
        }
    }

    q$("#loginas").onclick = function(ev){
        cache.remove("username");
        cache.remove("password");
        location.reload();
    }
}
InitLink();
// login
