
var Link = (function(root){

    var ws_server = "ws://localhost:8100/websocket"

    function Link(resource) {
        this.resource = resource;
        this.ws = null;
    }

    Link.prototype.connect = function(timeout){
        var that = this;
        if(timeout > 60) { timeout = 60; }
        console.log('connecting to ' + ws_server + '...');

        //new SockJS(ws_server)
        that.ws = new WebSocket(ws_server);
        that.ws.onopen = function () {
            console.log('WebSocket connected');
            var credentials = {
                auth: {
                    username: that.resource.player.username,
                    password: that.resource.player.password
                }
            };
            // that.player.onMessage
            that.ws.send(JSON.stringify(credentials));
        };
        that.ws.onmessage = function(event) {
            var msg = JSON.parse(event.data);
            console.log('WebSocket onMessage', msg);
            that.resource.onMessage(msg);
            // shared.scope.$emit('update', msg);
        };
        that.ws.onclose = function() {
            that.ws = null;
            console.log('WebSocket disconnected');
            setTimeout(function(){
                that.connect(timeout*2);
            }, timeout * 1000);
        };
    };

    Link.prototype.send = function(data){
        this.ws.send(JSON.stringify(data));
    };

//connect(1);
    return Link;

})(this);
