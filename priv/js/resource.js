
function Resource(username, password){
    this.username = username;
    this.password = password;
    this.player = new Player(username, password);
    this.players = new Players();
    this.rooms = new Rooms();
    this.room = new Room(player);
    this.resources = ['player', 'players', 'room', 'rooms'];
}

Resource.prototype.onMessage = function(msg){
    var that = this;
    // console.log("Resource msg [", msg, "]");

    _.map(this.resources, function(resource){
        // console.log("Map:", resource);
        if(_.has(that, resource), _.has(msg, resource)){
            that[resource].onMessage(msg[resource]);
        }
    });

}
