function Rooms() {
}

Rooms.prototype.onMessage = function(data){
    // console.log("Rooms data [", data, "]");
    var ul = q$("#rooms ul.list");
    var list = _.map(data, function(item){
        // var room = item.room || {state: "free"};
        return ''+
            '<li>' +
                '<span class="pid">' + item.pid + '</span>' +
                '<span class="connectat">' + formatDate(item.startat) + '</span>' +
                '<span class="owner">' + item.owner + '</span>' +
                '<span class="room">' + item.state + '</span>' +
            '</li>';
    }).join('');
    // console.log("List=", list);
    ul.innerHTML = list;
};


function Room(player) {
    this.player = player;
    this._info = {
        pid: q$("#room .room_pid"),
        owner: q$("#room .room_owner"),
        state: q$("#room .room_state"),
        players: q$("#room .room_players")
    };
    this.widget = q$("#room_widget");
}

Room.prototype.onMessage = function(data){
    var that = this;
    // console.log("Room data [", data, "]");
    that._info.pid.innerHTML = data.pid;
    that._info.owner.innerHTML = data.owner;
    that._info.state.innerHTML = data.state;
    that._info.players.innerHTML = data.players;

    if(_.has(data, 'data')){
        // Заполняем виджет.
        // В идеале процесс должен быть неразрушающим, изменять только новые данные.
        // Но пока, пересоздаем все.
        that.widget.innerHTML = '';
        // var widget = '';
        var content = _.forEach(data.data, function(el){
            // console.log('>> widget: ', el);
            if(_.has(that.constructor, el.type)){
                // widget += that.constructor[el.type](el.id, el.data);
                var element = that.constructor[el.type](el.id, el.data);
                that.widget.appendChild(element);
            }
        });
        // console.log('content', widget);
        // this.widget.innerHTML = widget;
    }
}

Room.titles = {
    'create_room': "Создать свою игру",
    'exit_room': "Выйти из игры",
    'game_rooms': "Список игр:",
    'owner': "Вы - создатель игры"
};

Room.commands = {
    'command_button': function(ev){
        // console.log("Room.command: 'command_button'", ev, ev.target.dataset.command);
        if(link){
            link.send({command: ev.target.dataset.command, target: ev.target.dataset.target});
        }
    }
};

Room.prototype.constructor = {

    'command_button': function(id, data) {
        // console.log('Construct Command_Button', id, data);
        var command = data.command;
        var title = Room.titles[command] || "Неопределено";
        var button = element('button');
        button.textContent = title;
        button.dataset.command = command;
        if(Room.commands['command_button'])
            button.addEventListener('click', Room.commands['command_button'], false);
        return button;
        // return '<button id="command" data-command="' + command + '">' + title + '.</button>';
    },
    'menu_list': function(id, data) {
        // console.log('Construct Rooms_List', id, data);
        var menu = element('div');
        var header = element('div');
        header.innerHTML = Room.titles[data.title];
        menu.appendChild(header);
        var menu_items = element('ul');
        menu.appendChild(menu_items);
        _.map(data.rooms, function(room){
            var menu_item = element('li');
            menu_item.innerHTML = 'Игра' + room.pid;
            menu_items.appendChild(menu_item);
            var button = element("button");
            button.textContent = "Присоединиться";
            button.dataset.command = "join_room";
            button.dataset.target = room.pid;
            button.addEventListener('click', Room.commands['command_button'], false);
            menu_item.appendChild(button);
            // return '<li>Игра' + room.pid + '</li>';
        });
        return menu;
    },
    'label': function(id, data) {
        console.log('Construct Label', id, data);
        var label = element('div');
        label.innerHTML = Room.titles[data.title];
        return label;
    }
};
