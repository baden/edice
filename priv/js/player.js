
function Player(username, password) {
    this.username = username;
    this.password = password;
    this._info = {
        username: q$("#player .player_username"),
        id: q$("#player .player_id"),
        state: q$("#player .player_state")
    };
}

Player.prototype.onMessage = function(data){
    // console.log("Player data [", data, "]");

    this._info.username.innerHTML = data.username;
    this._info.id.innerHTML = data.pid;
    this._info.state.innerHTML = data.state;
};

function formatDate(date) {
    return moment(new Date(date * 1000)).format("DD/MM/YYYY HH:mm:ss");
}


function Players() {
}

Players.prototype.onMessage = function(data){
    // console.log("Players data [", data, "]");
    var ul = q$("#players ul.list");
    var list = _.map(data, function(item){
        var room = item.room || "free";
        return ''+
            '<li>' +
                '<span class="connectat">' + formatDate(item.startat) + '</span>' +
                '<span class="username">' + item.username + '</span>' +
                '<span class="room">' + room + '</span>' +
            '</li>';
    }).join('');
    // console.log("List=", list);
    ul.innerHTML = list;
};
