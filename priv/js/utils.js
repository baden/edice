var q$ = function(selector){
    return document.querySelector(selector);
}

function Cache(session){
    this.storage = session ? sessionStorage : localStorage;
}

Cache.prototype = {
    get: function(key){
        return this.storage.getItem(key);
    },
    set: function(key, value){
        return this.storage.setItem(key, value);
    },
    remove: function(key){
        return this.storage.removeItem(key);
    }
};

var element = function(el){
    return document.createElement(el);
};
