# API

### Получить список игр, ожидающих начала

GET /games

Ответ

```json
{
  "id": "game_id",
  "map": "map_name"
}
```


### Создание игры

POST /game
