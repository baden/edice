.PHONY: deps

PROJECT=edice

REBAR=rebar

all: deps compile generate

compile: deps
	$(REBAR) compile

deps:
	$(REBAR) get-deps

clean:
	$(REBAR) clean delete-deps

generate:
	$(REBAR) generate

console:
	rel/$(PROJECT)/bin/$(PROJECT) console

update:
	$(REBAR) clean delete-deps get-deps compile generate

run:
	$(REBAR) compile skip_deps=true && \
	erl -pa ebin deps/*/ebin -s edice -s observer -s sync -config rel/files/sys.config
