'use strict';

describe('Directive: room', function () {

  // load the directive's module
  beforeEach(module('ediceApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<room></room>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the room directive');
  }));
});
