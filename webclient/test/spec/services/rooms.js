'use strict';

describe('Service: rooms', function () {

  // load the service's module
  beforeEach(module('ediceApp'));

  // instantiate service
  var rooms;
  beforeEach(inject(function (_rooms_) {
    rooms = _rooms_;
  }));

  it('should do something', function () {
    expect(!!rooms).toBe(true);
  });

});
