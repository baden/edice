'use strict';

/**
 * @ngdoc directive
 * @name ediceApp.directive:room
 * @description
 * # room
 */
angular.module('ediceApp')
  .directive('room-lobby', function () {

    return {
      template: '<div>This is lobby</div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        console.log('<room-lobby>', room, player);
      }
    };
  });
