'use strict';

/**
 * @ngdoc service
 * @name ediceApp.logger
 * @description
 * # logger
 * Factory in the ediceApp. Это по сути чат. Но также в чате отображаются и все события, ходы и т.п.
 */
angular.module('ediceApp')
  .factory('logger', function (ws) {
    // Service logic

    console.log('logger:constructor', this);

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
