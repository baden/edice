'use strict';

/**
 * @ngdoc service
 * @name ediceApp.ws
 * @description
 * # ws
 * Provider in the ediceApp.
 */
angular.module('ediceApp')
  .provider('ws', function (SERVER) {
    // Private variables
    // var port = 8100;
    var server = SERVER.ws || ("ws://" + location.hostname + ":8100/websocket");


    function Cache(session){
        this.storage = session ? sessionStorage : localStorage;
    }

    Cache.prototype = {
        get: function(key){
            return this.storage.getItem(key);
        },
        set: function(key, value){
            return this.storage.setItem(key, value);
        },
        remove: function(key){
            return this.storage.removeItem(key);
        }
    };

    var cache = new Cache(true);

    var username = cache.get("username");
    var password = cache.get("password");

    // Private constructor
    function Ws(scope, timeout) {

      var shared = this.shared = {
        scope: scope
      };

      var ws = null;

      var STATES = this.STATES = {
        'INIT': 0,          // Not set even credentials
        'DISCONNECTED': 1,  // Not connected (delay before connecting)
        'CONNECTING': 2,    // Connection in progress
        'CONNECTED': 3,     // Connected to server
        'NOAUTH': 4         // Нет авторизации, требуется ввод имени и пароля
      };
      var state = STATES['INIT'];

      var connect = function(timeout){
        if(!username) {
          state = STATES['NOAUTH'];
          return;
        }
        if(timeout > 60) { timeout = 60; }
        console.debug('connecting to ' + server + '...');
        state = STATES['CONNECTING'];

        //new SockJS(ws_server)
        ws = new WebSocket(server);
        ws.onopen = function () {
            state = STATES['CONNECTED'];
            console.info('WebSocket connected');
            var credentials = {
                auth: {
                    username: username,
                    password: password
                }
            };
            // that.player.onMessage
            ws.send(JSON.stringify(credentials));
        };
        ws.onmessage = function(event) {
            var msg = JSON.parse(event.data);
            console.info('WebSocket onMessage', msg);
            // that.resource.onMessage(msg);
            shared.scope.$emit('update', msg);
        };
        ws.onclose = function() {
            ws = null;
            state = STATES['DISCONNECTED'];
            console.warn('WebSocket disconnected');
            setTimeout(function(){
                connect(timeout*2);
            }, timeout * 1000);
        };
      };

      this.setCredentials = function (_username, _password) {
        console.log('ws:setCredentials and reconnect');
        username = _username;
        password = _password;
        cache.set("username", username);
        cache.set("password", password);
        // if(ws && ws.readyState === ws.OPEN){
        //   var credentials = {
        //       auth: {
        //           username: username,
        //           password: password
        //       }
        //   };
        //   ws.send(JSON.stringify(credentials));
        // }
      };

      // Убедимся что подключение к серверу будет после инициализации.
      timeout(function(){
        connect(1);
      }, 0);
      // scope.$apply(function(){
      // });

      this.credentials = function () {
        return {
          username: username,
          password: password
        }
      };



      this.state = function () {
        return state;
      }

      this.send = function(data) {
        if(ws && ws.readyState === ws.OPEN){
          ws.send(JSON.stringify(data));
        }
      }

      this.on = function(resource, callback) {
        return shared.scope.$on('update', function(event, message){
          // console.log('ws.on.update Resource=', resource, 'Event=', event, 'Message=', message);
          if(message[resource]){
            timeout(function(){
              callback(message[resource]);
            }, 0);
          }
        })
      }
    }

    // Public API for configuration
    this.setServer = function(_server) {
      server = _server;
    }
    this.setCredentials = function (_username, _password) {
      username = _username;
      password = _password;
      cache.set("username", username);
      cache.set("password", password);
    };

    // Method for instantiating
    this.$get = function ($rootScope, $timeout) {
      return new Ws($rootScope.$new(true), $timeout);
    };
  });
