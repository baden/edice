'use strict';

/**
 * @ngdoc service
 * @name ediceApp.resource
 * @description
 * # resource
 * Factory in the ediceApp.
 */
angular.module('ediceApp')
  .factory('resource', function (ws) {
    // Service logic

    var Resource = function(name) {
      var self = this;
      self.name = name;
      self.model = null;
      ws.on(name, function(message){
        // console.debug('Resource', name, ':update', message);
        self.model = message;
      });
    };

    Resource.prototype.update = function() {
      ws.send({update: this.name});
    }

    // Public API here
    return Resource;
  });
