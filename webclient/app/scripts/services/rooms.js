'use strict';

/**
 * @ngdoc service
 * @name ediceApp.rooms
 * @description
 * # rooms
 * Factory in the ediceApp.
 */
angular.module('ediceApp')
  .factory('rooms', function (resource) {
    // Service logic
    var Rooms = new resource('rooms');

    return Rooms;
  });
