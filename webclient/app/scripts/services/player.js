'use strict';

/**
 * @ngdoc service
 * @name ediceApp.player
 * @description
 * # player
 * Factory in the ediceApp.
 */
angular.module('ediceApp')
  .factory('player', function (resource) {
    // Service logic
    var Player = new resource('player');
    // Public API here
    return Player;
  });
