'use strict';

/**
 * @ngdoc service
 * @name ediceApp.players
 * @description
 * # players
 * Factory in the ediceApp.
 */
angular.module('ediceApp')
  .factory('players', function (resource) {
    // Service logic
    var Players = new resource('players');
    // Public API here
    return Players;
  });
