'use strict';

/**
 * @ngdoc service
 * @name ediceApp.room
 * @description
 * # room
 * Factory in the ediceApp.
 */
angular.module('ediceApp')
  .factory('room', function (resource) {
    // Service logic
    var Room = new resource('room');
    // Public API here
    return Room;
  });
