'use strict';

/**
 * @ngdoc directive
 * @name ediceApp.directive:ngenter
 * @description
 * # ngenter
 */
angular.module('ediceApp')
  .directive('chat', function (ws, players, player) {
    return {
      restrict: 'E',
      templateUrl: 'templates/chat.html',
      link: function postLink(scope, element, attrs) {

        scope.doChat = function(){
          console.log("doChat", scope.chat);
        };

        scope.okClick = function(ev){
          var target = ev.target;
          console.log("click", ev, target, $(target).hasClass("sender"));
          if($(target).hasClass("sender")){
            console.log("add name");
            var input = element.find("input");
            input[0].value += " " + target.innerText + " ";
            input.focus();
            // ev.
          }
        }

        var chatlog = element.find("pre");
        var chatcontainer = element.find(".chat-container");

        var cleanup = ws.on("chat", function(message){
          console.debug('Chat', message);
          var text = message.text;
          // var priv = false;
          // var me = player.model.username;
          angular.forEach(players.model, function(player){
            // if(text.match())
            // console.log("player=", player);
            var regexp = RegExp(player.username, "g");
            text = text.replace(regexp, '<span class="player">' + player.username + '</span>');
            // if(player.model.username == me) {
              // priv = true;
            // }
          });

          // var priv = "";
          // if(text.match(player.model.username)){
            // text = '<span class="private">' + text + "</span>";
          // }

          var line = $("<p></p>").append("<p><a></a><span class=\"sender\">" + message.sender.username + "</span>" + text + "</p>");
          // var line = chatlog.append("<p><a></a><span class=\"sender\">" + message.sender.username + "</span>" + text + "</p>");
          console.log("line=", line);
          if(text.match(player.model.username)){
            line.addClass("private");
          }
          chatlog.append(line);

          chatcontainer.stop();
          chatcontainer.animate({scrollTop: chatcontainer[0].scrollHeight}, 100);
          // chatcontainer[0].scrollTop = chatcontainer[0].scrollHeight;
        });

        scope.$on("$destroy", function() {
          console.log("on destroy", cleanup);
          cleanup();
        });

        console.log("chat link", element.find("input"));
        element.find("input").bind("keydown keypress", function (event) {
            if(event.which === 13) {
              // console.log("Enter", this.value);
              ws.send({chat: this.value});
              this.value = "";
              event.preventDefault();
            }
        });
      }
    };
  });
