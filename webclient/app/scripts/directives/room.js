'use strict';

/**
 * @ngdoc directive
 * @name ediceApp.directive:room
 * @description
 * # room
 */
angular.module('ediceApp')
  .directive('room', function (room, player, $templateCache, $http, $compile, $q, ws) {

    var getTemplate = function(url){
      var defer = $q.defer();
      $http.get(url, {cache: $templateCache}).
         success(function(content) {
           defer.resolve(content);
         });
      return defer.promise;
    }

    // var getTemplate = function(url){
    //   var defer = $q.defer();
    //   var template = $templateCache.get(url);
    //   if(template){
    //     console.log('load room template from cache', template);
    //     defer.resolve(template);
    //   } else {
    //     $http.get(url, {cache: $templateCache}).
    //     // $http.get(url).
    //       success(function(content) {
    //         console.log('load room template', content);
    //         defer.resolve(content);
    //       })
    //     // template = $templateRequest(url);
    //   }
    //   // return template;
    //   return defer.promise;
    // };

    return {
      template: '<div>Loading...</div>',
      // templateUrl: "rooms/undefined.html",
      restrict: 'E',
      // scope: {},
      link: function postLink(scope, element, attrs) {
        // console.log('<room>');
        scope.room = room;
        scope.player = player;
        scope.send = function(data) {
          console.log('Send:', data);
          ws.send(data);
        };

        scope.$watch('room.model', function(){
          // console.log('room was changed', room.model);
          // console.log('Try replace content');
          if(!room.model) return;
          var url = room.model.state;
          getTemplate("rooms/" + url + ".html").
            then(function(template){
              // console.log("template=", template);
              element.html(template);
              $compile(element.contents())(scope);
            });
        }, true);
        // element.text('this is the room directive');
      }
    };
  });
