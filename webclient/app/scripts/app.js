'use strict';

/**
 * @ngdoc overview
 * @name ediceApp
 * @description
 * # ediceApp
 *
 * Main module of the application.
 */
angular
  .module('ediceApp', [
    'ngAnimate',
    // 'ngCookies',
    'ngResource',
    'ngRoute',
    // 'ngSanitize',
    'ngTouch'
    // 'ngMaterial'
  ])
  .constant('SERVER', {
    'ws': "ws://" + location.hostname + ":8100/websocket"
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/players', {
        templateUrl: 'views/players.html',
        controller: 'PlayersCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/rooms', {
        templateUrl: 'views/rooms.html',
        controller: 'RoomsCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/room/:room', {
        templateUrl: function(param){
          console.log("room router", param);
          return 'rooms/lobby.html';
        },
        controller: 'MainCtrl'
      })
      .when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
