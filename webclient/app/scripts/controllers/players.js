'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:PlayersCtrl
 * @description
 * # PlayersCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('PlayersCtrl', function ($scope, players) {
    $scope.players = players;
    players.update();
  });
