'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('LoginCtrl', function ($scope, ws) {
    // $scope.user = {
    //   username: 'baden',
    //   password: '111'
    // }
    // console.log(ws);
    $scope.user = angular.copy(ws.credentials());
    $scope.onSubmit = function(){
      console.log('onSubmit', $scope.user);
      ws.setCredentials($scope.user.username, $scope.user.password);
      location.hash = "#/";
      location.reload();
    }
  });
