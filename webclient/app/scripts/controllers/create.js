'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:CreateCtrl
 * @description
 * # CreateCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('CreateCtrl', function ($scope, ws, $location) {
    $scope.params = {};

    $scope.onSubmit = function(){
      console.log('onSubmit', $scope.params);
      // ws.setCredentials($scope.user.username, $scope.user.password);
      ws.send({command: 'create_room', params: $scope.params});
      $location.path("/");
      $location.replace();  // Не дать вернуться на эту страницу по кнопке "назад"
      // location();
    }

  });
