'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:RoomsCtrl
 * @description
 * # RoomsCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('RoomsCtrl', function ($scope, rooms) {
    $scope.rooms = rooms;
    rooms.update();
  });
