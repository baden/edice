'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .config(function(wsProvider){
    // wsProvider.setCredentials('baden', '333');
  })
  .controller('AppCtrl', function ($scope, $location, $timeout, ws, players, rooms, player, room) {
    // $scope.location = $location;
    $scope.isPath = function(Pattern) {
      return $location.path().match(Pattern);
    }

    $scope.$on("$routeChangeStart", function (event, next, current) {
	    $scope.path = $location.path();
	  });

    $scope.players = players;
    $scope.rooms = rooms;
    $scope.player = player;
    $scope.room = room;
    $scope.logedin = function(){
      return ws.state() != ws.STATES['NOAUTH'];
    }
    $timeout(function(){
      if(ws.state() === ws.STATES['NOAUTH']) {
        $location.path("/login");
      }
    }, 0);
  });
