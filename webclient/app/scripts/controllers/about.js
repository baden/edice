'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
