'use strict';

/**
 * @ngdoc function
 * @name ediceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ediceApp
 */
angular.module('ediceApp')
  .controller('MainCtrl', function ($scope, player, room, $location) {
    // $scope.players = players;
    $scope.player = player;
    $scope.room = room;

    // $scope.$on('$routeChangeStart', function (event, next, current) {
    //   // do something
    //   console.log('Try hook back button', event, next, current);
    //   console.log('$location.path() = ', $location.path());
    //   // event.preventDefault();
    //   // $location.path("/rooms");
    //   // if($location.path() === "/create"){
    //   //   $location.path("/");
    //   // }
    // });
  });
