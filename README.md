# Свободная opensource альтернатива игры vkubiki.ru.

В самой начальной стадии разработки.

## Планируемые технологии:

### Сервер

Тут без вариантов - Erlang/OTP.

Планируется использовать минимум компонентов. В идеале - работа на голой [XEN-машине](http://erlangonxen.org/).
Поэтому никаких баз данных, брокеров сообщений и скорее всего даже без **nginx**.

Протоколы:

* HTTP, REST, Websocket - для HTML5 клиентов.
* TCP/IP - для нативных клиентов на Android, iOS, Windows, Linux и т.д.

Библиотеки:

cowboy - HTTP, REST, Websocket.
ranch - TCP/IP.

База данных - mnesia.

#### Присмотреться к библиотекам

* [Leptus is an Erlang REST framework that runs on top of cowboy.](https://github.com/s1n4/leptus)

#### Тестирование

Хоть это и не относится прямо к проекту, но чтобы не потерять:

* [Pretty EUnit test formatters](https://github.com/eproxus/unite)
* [A mocking library for Erlang.](https://github.com/eproxus/meck) - пример использования [тут](https://github.com/cloud8421/storm-collector/blob/b96663404a4b20b5e9906e2080fbd015e8236097/test/datapoint_builder_test.erl)


#### На грани

Шальная мысль - использовать исключительно R17, для поддержания прогресса, так сказать:

* [Erlang R17](http://joearms.github.io/2014/02/01/big-changes-to-erlang.html)
* [jsxn-для декодирования в map](https://github.com/talentdeficit/jsxn)

Или же можно использовать [jiffy](https://github.com/davisp/jiffy), он уже поддерживает maps. В этом плане он становится совместим с jsxn.

Нужно тольго глянуть, не сломается ли observer. Может и #state{} есть смысл заменить на map.


### HTML5 Клиент.

Компоненты:
- [AngularJS](https://angularjs.org/) - просто потому что хочется.
- Попробовал на телефоне [Angular Material Design](https://material.angularjs.org/#/) и [Paper Elements](https://www.polymer-project.org/components/paper-elements/demo.html#paper-checkbox). Первое работает по ощущениям чуть заторможено. Второе и работает поживее,
  и выглядит на порядок круче. Нужно исследовать совместимость технологии [polymer](https://www.polymer-project.org/).
- Карты [Leaflet](http://leafletjs.com/) идея из [оригинала](http://vkubiki.ru), но со свободной лицензией.

### Революционные отличия от оригинала.

Не хочется в точности повторять оригинал. Хочется внести какое-то разнообразие в игровой процесс.

Кандидаты:

1. Бонусы.
  * +1 к атаке.
  * +1 к защите.
  * Щит на 1 ход.
  * Диверсия (убирает 1 кубик перед атакой).
  * Артобстрел (убирает по 1 кубику на сопредельных территориях в случае победы).


2. Достижения. Это чтобы меряться пиписьками.
  * 10 побед подряд.
  *

### Социальная составляющая.

* Аватарки.
* Смайлы в чате.
* Подарки.

### Как следить за порядком?

Демократия. Модератор выбирается путем голосования. Срок? (неделя, месяц, стоит подумать над этим).

### Разработчикам

Запуск Erlang-сервера.

```bash
    make run
```

Запуск автосборки клиента

```bash
    grunt serve
```
